﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appppp
{
    public partial class Form2 : Form
    {
        List<queue> Motor = new List<queue>();
        string lastNumber;
        string State;
        int x;
        public Form2(List<queue> Motor, string number)
        {
            InitializeComponent();
            this.Motor = Motor;
            lastNumber = number;
            State = "Add";
        }
        public Form2(List<queue> Motor,int x)
        {
            InitializeComponent();
            this.Motor = Motor;
            State = "Edit";
            this.x = x;
            button1.Text = "Edit step";
        }
        private void Add_to_queue(object sender, EventArgs e)
        {
            if ((textBox1.Text.Length == 0) || (textBox2.Text.Length == 0) || (textBox4.Text.Length == 0) || (textBox5.Text.Length == 0))
            {
                string v = "Не заполнены поля:";
                if (textBox1.Text.Length == 0)
                {
                    v += " 'Enter a number of motor';";
                    label11.Visible = true;
                }
                if (textBox2.Text.Length == 0)
                {
                    v += " 'Set a step';";
                    label7.Visible = true;

                }
                if (textBox4.Text.Length == 0)
                {
                    v += " 'Setup speed';";
                    label9.Visible = true;
                }
                if (textBox5.Text.Length == 0)
                {
                    v += " 'Steup a work speed';";
                    label10.Visible = true;
                }
                MessageBox.Show(v);
            }
            else
            {
                if (State == "Add")
                {
                    queue newMotor = new queue();
                    newMotor.addMotor(lastNumber, textBox1.Text.ToString(), textBox2.Text.ToString(), textBox3.Text.ToString(), textBox4.Text.ToString(), textBox5.Text.ToString());
                    Motor.Add(newMotor);
                }
                else
                {
                    queue newMotor = new queue();
                    newMotor.addMotor(Motor[x].number.ToString(), textBox1.Text.ToString(), textBox2.Text.ToString(), textBox3.Text.ToString(), textBox4.Text.ToString(), textBox5.Text.ToString());
                    Motor[x] = newMotor;
                }




                this.Hide();
            }
        }

        private void Save_values_​​from_listbox(object sender, EventArgs e)
        {
            textBox1.Text = "";
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    textBox1.Text += checkedListBox1.Items[i].ToString();
                    textBox1.Text += ";";
                }

            }




        }
        private void TextBox1_Enter(object sender, EventArgs e)
        {
            label11.Visible = false;
        }
        private void TextBox2_Enter(object sender, EventArgs e)
        {
            label7.Visible = false;
        }

        private void TextBox4_Enter(object sender, EventArgs e)
        {
            label9.Visible = false;
        }

        private void TextBox5_Enter(object sender, EventArgs e)
        {
            label10.Visible = false;
        }

        private void TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
                e.Handled = true;
        }
    }
}
