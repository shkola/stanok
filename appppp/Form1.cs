﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace appppp
{
    public partial class Form1 : Form
    {
        List<List<int[]>> data = new List<List<int[]>>();
        List< queue> Motor = new List<queue>();


      /* string path = @"D:\Proekt\appppp\appppp\data.txt";
        public void ReadFromFile() {
        FileStream file1 = new FileStream(path, FileMode.Open,FileAccess.Write);
        StreamWriter reader = new StreamWriter(file1);
        string line;
            reader.WriteLine(path);
            foreach (queue data in Motor)
            {
                line=
                //dataGridView1.Rows.Add(data.number, data.nmotorToString(), data.step, data.dir, data.speed, data.workspeed);
            }
            reader.Close();
            file1.Close();


                /*while ((line = reader.ReadLine()) != null)
                    {
                     Console.WriteLine(line);
                     }        
             

        }*/
        public Form1()
        {
            InitializeComponent();
            
            updateData();
            colorMotor();
            

            
           
        }

      public void colorMotor() {
            Dictionary<int, Color> motorcolor = new Dictionary<int, Color>
            {
                { 0, Color.Red },
                { 1, Color.Blue },
                { 2, Color.Orange },
                { 3, Color.Black },
                { 4, Color.Pink },
                { 5, Color.Green },
                { 6, Color.Yellow },
                { 7, Color.Brown },
                { 8, Color.Purple },
                { 9, Color.Olive }
            };
            Panel[] arr = new Panel[10];

            int i = 0;
            arr[i] = panel1;
            arr[++i] = panel2;
            arr[++i] = panel3;
            arr[++i] = panel4;
            arr[++i] = panel5;
            arr[++i] = panel6;
            arr[++i] = panel7;
            arr[++i] = panel8;
            arr[++i] = panel9;
            arr[++i] = panel10;
            for (i = 0; i < 10; i++)
            {
                Color color = motorcolor[i];
                arr[i].BackColor = color;
            }


        }
        public void updateData() {
            dataGridView1.Rows.Clear();
            foreach (queue data in Motor) { 
            
                    dataGridView1.Rows.Add(data.number,data.nmotorToString(),data.step,data.dir,data.speed,data.workspeed);
             
            }
        }


        private void Button_EXIT(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void New_form_addstep(object sender, EventArgs e)
        {

            Form2 form2 = new Form2(Motor, dataGridView1.Rows.Count.ToString());
            form2.ShowDialog();
           
            updateData();

        }

        private void Instruction1(object sender, EventArgs e)
        {
            var xlApp = new Excel.Application();
            xlApp.Visible = true;
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            xlApp.Workbooks.Open(path + @"\Инструкция к ПРУЖИННОМУ.xls");
        }

        private void Instruction2(object sender, EventArgs e)
        {
            var xlApp = new Excel.Application();
            xlApp.Visible = true;
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            xlApp.Workbooks.Open(path + @"\Пример Дужки.xlsx");
        }

        private void Instruction3(object sender, EventArgs e)
        {
            var xlApp = new Excel.Application();
            xlApp.Visible = true;
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            xlApp.Workbooks.Open(path + @"\Пример Пружины.xlsx");
        }

        private void Delete_selected_row(object sender, EventArgs e)
        {
            try
            {
                Motor.RemoveAt(dataGridView1.SelectedRows[0].Index);
                dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
                int i = 1;
                foreach (queue data in Motor)
                {
                    data.number = i;
                    i++;
                }
                updateData();
            }
            catch (Exception){ MessageBox.Show("You have not selected a row!!"); }
        }

        private void Edit_Selected_Row(object sender, EventArgs e)
        {
            try
            {
                Form2 form2 = new Form2(Motor, dataGridView1.SelectedRows[0].Index);
                form2.ShowDialog();

                updateData();
            }
            catch (Exception){ MessageBox.Show("You have not selected a row!!"); }
        }
    }
}
